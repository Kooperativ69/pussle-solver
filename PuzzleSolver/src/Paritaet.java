import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Paritaet {
	
	static int MAX_X = 3;
	static int MAX_Y = 3;
	static int MIN_Y = 0;
	static int MIN_X = 0;
	
	public static ArrayList<Integer> getPariteat(Integer[] [] array)
	{
		ArrayList<Integer> p = new ArrayList<>();

		for(int x = 0; x < array.length; x++)
			for(int y = 0; y < array.length; y++)
				p.add(array[x] [y]);
			
		return p;
	}
	
	public static Integer[] getLeerfeld(Integer[] [] array)
	{
		int X = 0, Y = 0;
		
		
		for(int x = 0; x < array.length; x++)
			for(int y = 0; y < array.length; y++) {
				if(array[x] [y] == array.length*array.length) {
					X = x;
					Y = y;
				}
			}
		
		Integer[] f = new Integer[2];
		
		f[0] = X;
		f[1] = Y;
		
		return f;
	}
	
	public static ArrayList<Integer[]> getChangeableFields(Integer[] [] array)
	{
		final Integer[] leerfeld = getLeerfeld(array);
		
		MAX_X = array.length-1;
		MAX_Y = array.length-1;
		
		ArrayList<Integer[]> integers = new ArrayList<>();
		if(leerfeld[0] == MAX_X)
		{
			if(leerfeld[1] == MAX_Y)
			{
				integers.add(new Integer[]{MAX_X, MAX_Y-1});
				integers.add(new Integer[]{MAX_X-1, MAX_Y});
			
			}
			else if(leerfeld[1] == MIN_Y)
			{
				integers.add(new Integer[]{MAX_X, MIN_Y+1});
				integers.add(new Integer[]{MAX_X-1, MIN_Y});
			}
			else
			{
				integers.add(new Integer[]{MAX_X, leerfeld[1]-1});
				integers.add(new Integer[]{MAX_X, leerfeld[1]+1});
				integers.add(new Integer[]{MAX_X-1, leerfeld[1]});
			}
		}
		else if(leerfeld[0] == MIN_X)
		{
			if(leerfeld[1] == MAX_Y)
			{
				integers.add(new Integer[]{MIN_X, MAX_Y-1});
				integers.add(new Integer[]{MIN_X+1, MAX_Y});
			
			}
			else if(leerfeld[1] == MIN_Y)
			{
				integers.add(new Integer[]{MIN_X, MIN_Y+1});
				integers.add(new Integer[]{MIN_X+1, MIN_Y});
			}
			else
			{
				integers.add(new Integer[]{MIN_X, leerfeld[1]-1});
				integers.add(new Integer[]{MIN_X, leerfeld[1]+1});
				integers.add(new Integer[]{MIN_X+1, leerfeld[1]});
			}
		}
		else
		{
			if(leerfeld[1] == MAX_Y)
			{
				integers.add(new Integer[]{leerfeld[0]-1, MAX_Y});
				integers.add(new Integer[]{leerfeld[0]+1, MAX_Y});
				integers.add(new Integer[]{leerfeld[0], MAX_Y-1});
			}
			else if(leerfeld[1] == MIN_Y)
			{
				integers.add(new Integer[]{leerfeld[0]-1, MIN_Y});
				integers.add(new Integer[]{leerfeld[0]+1, MIN_Y});
				integers.add(new Integer[]{leerfeld[0], MIN_Y+1});
			}
			else
			{
				integers.add(new Integer[]{leerfeld[0]-1, leerfeld[1]});
				integers.add(new Integer[]{leerfeld[0]+1, leerfeld[1]});
				integers.add(new Integer[]{leerfeld[0], leerfeld[1]+1});
				integers.add(new Integer[]{leerfeld[0], leerfeld[1]-1});
			}
		}
		return integers;
	}
	
	public static int getDistance(Integer[] [] array, int val)
	{
		Integer[] tocalc = new Integer[]{999,999};
		Integer[] original = new Integer[]{999,999};
		
		Integer[] [] finished = fillFinished(array.length);
		
		for(int x = 0; x < array.length; x++)
			for(int y = 0; y < array.length; y++) {
				if(array[x] [y] == val) {
					tocalc = new Integer[]{x,y};
				}
			}
		
		for(int x = 0; x < finished.length; x++)
			for(int y = 0; y < finished.length; y++) {
				if(finished[x] [y] == val) {
					original = new Integer[]{x,y};
					break;
				}
			}
		
		int distance = Math.abs(tocalc[0]-original[0])+Math.abs(tocalc[1]-original[1]);
		return distance;
	}
	
	
	public static Integer[] [] fillFinished(int length) {
		Integer[] [] finished = new Integer[length][length];
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		numbers.addAll(IntStream.range(1, ((length*length)+1)).boxed().collect(Collectors.toList()));
		int index = 0;
		for(int x = 0; x < length; x++)
			for(int y = 0; y < length; y++) {
				int current = numbers.get(index);
				finished[x] [y] = current;
				index++;
			}	
		return finished;
		
	}

}

