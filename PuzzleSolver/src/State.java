import java.util.Arrays;


public class State implements Comparable<State> {

	public int g;
	public int f;
	public int id;
	public Integer[] [] state;
	public State parent;
	
	public State(Integer[] [] state, int g, int id, State parent)
	{
		this.g = g;
		transferState(state);
		f = getManDistance();
		this.id = id;
		this.parent = parent;
	}

	private void transferState(Integer[] [] state) {
		this.state = new Integer[state.length][state.length];
		for(int x = 0; x < state.length; x++)
			for(int y = 0; y < state.length; y++) {
				this.state[x] [y] = state[x] [y];
			}
	}
	
	public int getManDistance()
	{
		int dis = 0;
		for(int i = 1; i < state.length*state.length+1;i++)
			dis+=Paritaet.getDistance(state, i);
		return dis;
	}	
	public int getDistance()
	{
		return g+f;
	}

	@Override
	public int hashCode() {

		return getDistance();
	}

	@Override
	public boolean equals(Object obj) {
		int anz = 0;
		
		for(int x = 0; x < state.length; x++)
			for(int y = 0; y < state.length; y++) {
				if(state[x] [y] == ((State)(obj)).state[x] [y])
					anz++;
			}
		 
		if(anz == state.length*state.length) 
			return true;
		return false;
	}

	@Override
	public int compareTo(State o) {
		
		return Integer.compare(this.getDistance(), o.getDistance());
	}
	
	
	
}
