
public class Main {

	public static void main(String[] args) {

		long time1 = System.currentTimeMillis();
		PlayGround g = new PlayGround(3);
		
		System.out.println(g.getField(0));
		System.out.println(g.getField(1));
		g.solve(160000);
		long time2 = System.currentTimeMillis();
		
		System.out.println("It took us: "+(time2-time1) + " Milliseconds ");

	}

}